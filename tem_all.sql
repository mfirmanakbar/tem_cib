-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2017 at 02:48 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tem_all`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily`
--

CREATE TABLE `daily` (
  `id` int(5) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(64) NOT NULL,
  `amount` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id_guest` int(11) NOT NULL,
  `name_guest` varchar(255) DEFAULT NULL,
  `email_guest` varchar(255) DEFAULT NULL,
  `message_guest` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`id_guest`, `name_guest`, `email_guest`, `message_guest`) VALUES
(1, 'Fadri', 'fadridxdxdarx@gmail.com', 'Rest Fuell Berhasil'),
(2, 'Test', 'test@yahoo.com', 'blablabla');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title`, `description`) VALUES
(3, 'tes ', 'jcacbaisci'),
(4, 'm', 'm'),
(5, 'm', 'm'),
(6, 'm', 'm'),
(7, 'm', 'm'),
(8, 'm', 'm'),
(9, 'm', 'm'),
(10, 'm', 'ms'),
(12, 'tes ', 'jcacbaisci'),
(13, 'm', 'm'),
(14, 'm', 'm'),
(15, 'm', 'm'),
(16, 'm', 'm'),
(17, 'm', 'm'),
(18, 'm', 'm'),
(19, 'm', 'ms'),
(20, 'tes ', 'jcacbaisci'),
(21, 'm', 'm'),
(22, 'm', 'm'),
(23, 'm', 'm'),
(24, 'm', 'm'),
(25, 'm', 'm'),
(26, 'm', 'm'),
(27, 'm', 'ms'),
(28, 'tes ', 'jcacbaisci'),
(29, 'm', 'm'),
(30, 'm', 'm'),
(31, 'm', 'm'),
(32, 'm', 'm'),
(33, 'm', 'm'),
(34, 'm', 'm'),
(35, 'm', 'ms');

-- --------------------------------------------------------

--
-- Table structure for table `kota_kabupaten`
--

CREATE TABLE `kota_kabupaten` (
  `kota_id` smallint(11) NOT NULL,
  `kota_kabupaten` varchar(100) NOT NULL,
  `propinsi_id` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota_kabupaten`
--

INSERT INTO `kota_kabupaten` (`kota_id`, `kota_kabupaten`, `propinsi_id`) VALUES
(1, 'Kabupaten Aceh Barat', 1),
(2, 'Kabupaten Aceh Barat Daya', 1),
(3, 'Kabupaten Aceh Besar', 1),
(4, 'Kabupaten Aceh Jaya', 1),
(5, 'Kabupaten Aceh Selatan', 1),
(6, 'Kabupaten Aceh Singkil', 1),
(7, 'Kabupaten Aceh Tamiang', 1),
(8, 'Kabupaten Aceh Tengah', 1),
(9, 'Kabupaten Aceh Tenggara', 1),
(10, 'Kabupaten Aceh Timur', 1),
(11, 'Kabupaten Aceh Utara', 1),
(12, 'Kabupaten Bener Meriah', 1),
(13, 'Kabupaten Bireuen', 1),
(14, 'Kabupaten Gayo Lues', 1),
(15, 'Kabupaten Nagan Raya', 1),
(16, 'Kabupaten Pidie', 1),
(17, 'Kabupaten Pidie Jaya', 1),
(18, 'Kabupaten Simeulue', 1),
(19, 'Kota Banda Aceh', 1),
(20, 'Kota Langsa', 1),
(21, 'Kota Lhokseumawe', 1),
(22, 'Kota Sabang', 1),
(23, 'Kota Subulussalam', 1),
(1, 'Kabupaten Asahan', 2),
(2, 'Kabupaten Batu Bara', 2),
(3, 'Kabupaten Dairi', 2),
(4, 'Kabupaten Deli Serdang', 2),
(5, 'Kabupaten Humbang Hasundutan', 2),
(6, 'Kabupaten Karo', 2),
(7, 'Kabupaten Labuhanbatu', 2),
(8, 'Kabupaten Labuhanbatu Selatan', 2),
(9, 'Kabupaten Labuhanbatu Utara', 2),
(10, 'Kabupaten Langkat', 2),
(11, 'Kabupaten Mandailing Natal', 2),
(12, 'Kabupaten Nias', 2),
(13, 'Kabupaten Nias Barat', 2),
(14, 'Kabupaten Nias Selatan', 2),
(15, 'Kabupaten Nias Utara', 2),
(16, 'Kabupaten Padang Lawas', 2),
(17, 'Kabupaten Padang Lawas Utara', 2),
(18, 'Kabupaten Pakpak Bharat', 2),
(19, 'Kabupaten Samosir', 2),
(20, 'Kabupaten Serdang Bedagai', 2),
(21, 'Kabupaten Simalungun', 2),
(22, 'Kabupaten Tapanuli Selatan', 2),
(23, 'Kabupaten Tapanuli Tengah', 2),
(24, 'Kabupaten Tapanuli Utara', 2),
(25, 'Kabupaten Toba Samosir', 2),
(26, 'Kota Binjai', 2),
(27, 'Kota Gunung Sitoli', 2),
(28, 'Kota Medan', 2),
(29, 'Kota Padang Sidempuan', 2),
(30, 'Kota Pematangsiantar', 2),
(31, 'Kota Sibolga', 2),
(32, 'Kota Tanjung Balai', 2),
(33, 'Kota Tebing Tinggi', 2),
(1, 'Kabupaten Bengkulu Selatan', 3),
(2, 'Kabupaten Bengkulu Tengah', 3),
(3, 'Kabupaten Bengkulu Utara', 3),
(4, 'Kabupaten Benteng', 3),
(5, 'Kabupaten Kaur', 3),
(6, 'Kabupaten Kepahiang', 3),
(7, 'Kabupaten Lebong', 3),
(8, 'Kabupaten Mukomuko', 3),
(9, 'Kabupaten Rejang Lebong', 3),
(10, 'Kabupaten Seluma', 3),
(11, 'Kota Bengkulu', 3),
(1, 'Kabupaten Batang Hari', 4),
(2, 'Kabupaten Bungo', 4),
(3, 'Kabupaten Kerinci', 4),
(4, 'Kabupaten Merangin', 4),
(5, 'Kabupaten Muaro Jambi', 4),
(6, 'Kabupaten Sarolangun', 4),
(7, 'Kabupaten Tanjung Jabung Barat', 4),
(8, 'Kabupaten Tanjung Jabung Timur', 4),
(9, 'Kabupaten Tebo', 4),
(10, 'Kota Jambi', 4),
(11, 'Kota Sungai Penuh', 4),
(1, 'Kabupaten Bengkalis', 5),
(2, 'Kabupaten Indragiri Hilir', 5),
(3, 'Kabupaten Indragiri Hulu', 5),
(4, 'Kabupaten Kampar', 5),
(5, 'Kabupaten Kuantan Singingi', 5),
(6, 'Kabupaten Pelalawan', 5),
(7, 'Kabupaten Rokan Hilir', 5),
(8, 'Kabupaten Rokan Hulu', 5),
(9, 'Kabupaten Siak', 5),
(10, 'Kota Pekanbaru', 5),
(11, 'Kota Dumai', 5),
(12, 'Kabupaten Kepulauan Meranti', 5),
(1, 'Kabupaten Agam', 6),
(2, 'Kabupaten Dharmasraya', 6),
(3, 'Kabupaten Kepulauan Mentawai', 6),
(4, 'Kabupaten Lima Puluh Kota', 6),
(5, 'Kabupaten Padang Pariaman', 6),
(6, 'Kabupaten Pasaman', 6),
(7, 'Kabupaten Pasaman Barat', 6),
(8, 'Kabupaten Pesisir Selatan', 6),
(9, 'Kabupaten Sijunjung', 6),
(10, 'Kabupaten Solok', 6),
(11, 'Kabupaten Solok Selatan', 6),
(12, 'Kabupaten Tanah Datar', 6),
(13, 'Kota Bukittinggi', 6),
(14, 'Kota Padang', 6),
(15, 'Kota Padangpanjang', 6),
(16, 'Kota Pariaman', 6),
(17, 'Kota Payakumbuh', 6),
(18, 'Kota Sawahlunto', 6),
(19, 'Kota Solok', 6),
(1, 'Kabupaten Banyuasin', 7),
(2, 'Kabupaten Empat Lawang', 7),
(3, 'Kabupaten Lahat', 7),
(4, 'Kabupaten Muara Enim', 7),
(5, 'Kabupaten Musi Banyuasin', 7),
(6, 'Kabupaten Musi Rawas', 7),
(7, 'Kabupaten Ogan Ilir', 7),
(8, 'Kabupaten Ogan Komering Ilir', 7),
(9, 'Kabupaten Ogan Komering Ulu', 7),
(10, 'Kabupaten Ogan Komering Ulu Selatan', 7),
(11, 'Kabupaten Ogan Komering Ulu Timur', 7),
(12, 'Kota Lubuklinggau', 7),
(13, 'Kota Pagar Alam', 7),
(14, 'Kota Palembang', 7),
(15, 'Kota Prabumulih', 7),
(1, 'Kabupaten Lampung Barat', 8),
(2, 'Kabupaten Lampung Selatan', 8),
(3, 'Kabupaten Lampung Tengah', 8),
(4, 'Kabupaten Lampung Timur', 8),
(5, 'Kabupaten Lampung Utara', 8),
(6, 'Kabupaten Mesuji', 8),
(7, 'Kabupaten Pesawaran', 8),
(8, 'Kabupaten Pringsewu', 8),
(9, 'Kabupaten Tanggamus', 8),
(10, 'Kabupaten Tulang Bawang', 8),
(11, 'Kabupaten Tulang Bawang Barat', 8),
(12, 'Kabupaten Way Kanan', 8),
(13, 'Kota Bandar Lampung', 8),
(14, 'Kota Metro', 8),
(1, 'Kabupaten Bangka', 9),
(2, 'Kabupaten Bangka Barat', 9),
(3, 'Kabupaten Bangka Selatan', 9),
(4, 'Kabupaten Bangka Tengah', 9),
(5, 'Kabupaten Belitung', 9),
(6, 'Kabupaten Belitung Timur', 9),
(7, 'Kota Pangkal Pinang', 9),
(1, 'Kabupaten Bintan', 10),
(2, 'Kabupaten Karimun', 10),
(3, 'Kabupaten Kepulauan Anambas', 10),
(4, 'Kabupaten Lingga', 10),
(5, 'Kabupaten Natuna', 10),
(6, 'Kota Batam', 10),
(7, 'Kota Tanjung Pinang', 10),
(1, 'Kabupaten Lebak', 11),
(2, 'Kabupaten Pandeglang', 11),
(3, 'Kabupaten Serang', 11),
(4, 'Kabupaten Tangerang', 11),
(5, 'Kota Cilegon', 11),
(6, 'Kota Serang', 11),
(7, 'Kota Tangerang', 11),
(8, 'Kota Tangerang Selatan', 11),
(1, 'Kabupaten Bandung', 12),
(2, 'Kabupaten Bandung Barat', 12),
(3, 'Kabupaten Bekasi', 12),
(4, 'Kabupaten Bogor', 12),
(5, 'Kabupaten Ciamis', 12),
(6, 'Kabupaten Cianjur', 12),
(7, 'Kabupaten Cirebon', 12),
(8, 'Kabupaten Garut', 12),
(9, 'Kabupaten Indramayu', 12),
(10, 'Kabupaten Karawang', 12),
(11, 'Kabupaten Kuningan', 12),
(12, 'Kabupaten Majalengka', 12),
(13, 'Kabupaten Purwakarta', 12),
(14, 'Kabupaten Subang', 12),
(15, 'Kabupaten Sukabumi', 12),
(16, 'Kabupaten Sumedang', 12),
(17, 'Kabupaten Tasikmalaya', 12),
(18, 'Kota Bandung', 12),
(19, 'Kota Banjar', 12),
(20, 'Kota Bekasi', 12),
(21, 'Kota Bogor', 12),
(22, 'Kota Cimahi', 12),
(23, 'Kota Cirebon', 12),
(24, 'Kota Depok', 12),
(25, 'Kota Sukabumi', 12),
(26, 'Kota Tasikmalaya', 12),
(1, 'Kabupaten Administrasi Kepulauan Seribu', 13),
(2, 'Kota Administrasi Jakarta Barat', 13),
(3, 'Kota Administrasi Jakarta Pusat', 13),
(4, 'Kota Administrasi Jakarta Selatan', 13),
(5, 'Kota Administrasi Jakarta Timur', 13),
(6, 'Kota Administrasi Jakarta Utara', 13),
(1, 'Kabupaten Banjarnegara', 14),
(2, 'Kabupaten Banyumas', 14),
(3, 'Kabupaten Batang', 14),
(4, 'Kabupaten Blora', 14),
(5, 'Kabupaten Boyolali', 14),
(6, 'Kabupaten Brebes', 14),
(7, 'Kabupaten Cilacap', 14),
(8, 'Kabupaten Demak', 14),
(9, 'Kabupaten Grobogan', 14),
(10, 'Kabupaten Jepara', 14),
(11, 'Kabupaten Karanganyar', 14),
(12, 'Kabupaten Kebumen', 14),
(13, 'Kabupaten Kendal', 14),
(14, 'Kabupaten Klaten', 14),
(15, 'Kabupaten Kudus', 14),
(16, 'Kabupaten Magelang', 14),
(17, 'Kabupaten Pati', 14),
(18, 'Kabupaten Pekalongan', 14),
(19, 'Kabupaten Pemalang', 14),
(20, 'Kabupaten Purbalingga', 14),
(21, 'Kabupaten Purworejo', 14),
(22, 'Kabupaten Rembang', 14),
(23, 'Kabupaten Semarang', 14),
(24, 'Kabupaten Sragen', 14),
(25, 'Kabupaten Sukoharjo', 14),
(26, 'Kabupaten Tegal', 14),
(27, 'Kabupaten Temanggung', 14),
(28, 'Kabupaten Wonogiri', 14),
(29, 'Kabupaten Wonosobo', 14),
(30, 'Kota Magelang', 14),
(31, 'Kota Pekalongan', 14),
(32, 'Kota Salatiga', 14),
(33, 'Kota Semarang', 14),
(34, 'Kota Surakarta', 14),
(35, 'Kota Tegal', 14),
(1, 'Kabupaten Bangkalan', 15),
(2, 'Kabupaten Banyuwangi', 15),
(3, 'Kabupaten Blitar', 15),
(4, 'Kabupaten Bojonegoro', 15),
(5, 'Kabupaten Bondowoso', 15),
(6, 'Kabupaten Gresik', 15),
(7, 'Kabupaten Jember', 15),
(8, 'Kabupaten Jombang', 15),
(9, 'Kabupaten Kediri', 15),
(10, 'Kabupaten Lamongan', 15),
(11, 'Kabupaten Lumajang', 15),
(12, 'Kabupaten Madiun', 15),
(13, 'Kabupaten Magetan', 15),
(14, 'Kabupaten Malang', 15),
(15, 'Kabupaten Mojokerto', 15),
(16, 'Kabupaten Nganjuk', 15),
(17, 'Kabupaten Ngawi', 15),
(18, 'Kabupaten Pacitan', 15),
(19, 'Kabupaten Pamekasan', 15),
(20, 'Kabupaten Pasuruan', 15),
(21, 'Kabupaten Ponorogo', 15),
(22, 'Kabupaten Probolinggo', 15),
(23, 'Kabupaten Sampang', 15),
(24, 'Kabupaten Sidoarjo', 15),
(25, 'Kabupaten Situbondo', 15),
(26, 'Kabupaten Sumenep', 15),
(27, 'Kabupaten Trenggalek', 15),
(28, 'Kabupaten Tuban', 15),
(29, 'Kabupaten Tulungagung', 15),
(30, 'Kota Batu', 15),
(31, 'Kota Blitar', 15),
(32, 'Kota Kediri', 15),
(33, 'Kota Madiun', 15),
(34, 'Kota Malang', 15),
(35, 'Kota Mojokerto', 15),
(36, 'Kota Pasuruan', 15),
(37, 'Kota Probolinggo', 15),
(38, 'Kota Surabaya', 15),
(1, 'Kabupaten Bantul', 16),
(2, 'Kabupaten Gunung Kidul', 16),
(3, 'Kabupaten Kulon Progo', 16),
(4, 'Kabupaten Sleman', 16),
(5, 'Kota Yogyakarta', 16),
(1, 'Kabupaten Badung', 17),
(2, 'Kabupaten Bangli', 17),
(3, 'Kabupaten Buleleng', 17),
(4, 'Kabupaten Gianyar', 17),
(5, 'Kabupaten Jembrana', 17),
(6, 'Kabupaten Karangasem', 17),
(7, 'Kabupaten Klungkung', 17),
(8, 'Kabupaten Tabanan', 17),
(9, 'Kota Denpasar', 17),
(1, 'Kabupaten Bima', 18),
(2, 'Kabupaten Dompu', 18),
(3, 'Kabupaten Lombok Barat', 18),
(4, 'Kabupaten Lombok Tengah', 18),
(5, 'Kabupaten Lombok Timur', 18),
(6, 'Kabupaten Lombok Utara', 18),
(7, 'Kabupaten Sumbawa', 18),
(8, 'Kabupaten Sumbawa Barat', 18),
(9, 'Kota Bima', 18),
(10, 'Kota Mataram', 18),
(1, 'Kabupaten Kupang', 19),
(2, 'Kabupaten Timor Tengah Selatan', 19),
(3, 'Kabupaten Timor Tengah Utara', 19),
(4, 'Kabupaten Belu', 19),
(5, 'Kabupaten Alor', 19),
(6, 'Kabupaten Flores Timur', 19),
(7, 'Kabupaten Sikka', 19),
(8, 'Kabupaten Ende', 19),
(9, 'Kabupaten Ngada', 19),
(10, 'Kabupaten Manggarai', 19),
(11, 'Kabupaten Sumba Timur', 19),
(12, 'Kabupaten Sumba Barat', 19),
(13, 'Kabupaten Lembata', 19),
(14, 'Kabupaten Rote Ndao', 19),
(15, 'Kabupaten Manggarai Barat', 19),
(16, 'Kabupaten Nagekeo', 19),
(17, 'Kabupaten Sumba Tengah', 19),
(18, 'Kabupaten Sumba Barat Daya', 19),
(19, 'Kabupaten Manggarai Timur', 19),
(20, 'Kabupaten Sabu Raijua', 19),
(21, 'Kota Kupang', 19),
(1, 'Kabupaten Bengkayang', 20),
(2, 'Kabupaten Kapuas Hulu', 20),
(3, 'Kabupaten Kayong Utara', 20),
(4, 'Kabupaten Ketapang', 20),
(5, 'Kabupaten Kubu Raya', 20),
(6, 'Kabupaten Landak', 20),
(7, 'Kabupaten Melawi', 20),
(8, 'Kabupaten Pontianak', 20),
(9, 'Kabupaten Sambas', 20),
(10, 'Kabupaten Sanggau', 20),
(11, 'Kabupaten Sekadau', 20),
(12, 'Kabupaten Sintang', 20),
(13, 'Kota Pontianak', 20),
(14, 'Kota Singkawang', 20),
(1, 'Kabupaten Balangan', 21),
(2, 'Kabupaten Banjar', 21),
(3, 'Kabupaten Barito Kuala', 21),
(4, 'Kabupaten Hulu Sungai Selatan', 21),
(5, 'Kabupaten Hulu Sungai Tengah', 21),
(6, 'Kabupaten Hulu Sungai Utara', 21),
(7, 'Kabupaten Kotabaru', 21),
(8, 'Kabupaten Tabalong', 21),
(9, 'Kabupaten Tanah Bumbu', 21),
(10, 'Kabupaten Tanah Laut', 21),
(11, 'Kabupaten Tapin', 21),
(12, 'Kota Banjarbaru', 21),
(13, 'Kota Banjarmasin', 21),
(1, 'Kabupaten Barito Selatan', 22),
(2, 'Kabupaten Barito Timur', 22),
(3, 'Kabupaten Barito Utara', 22),
(4, 'Kabupaten Gunung Mas', 22),
(5, 'Kabupaten Kapuas', 22),
(6, 'Kabupaten Katingan', 22),
(7, 'Kabupaten Kotawaringin Barat', 22),
(8, 'Kabupaten Kotawaringin Timur', 22),
(9, 'Kabupaten Lamandau', 22),
(10, 'Kabupaten Murung Raya', 22),
(11, 'Kabupaten Pulang Pisau', 22),
(12, 'Kabupaten Sukamara', 22),
(13, 'Kabupaten Seruyan', 22),
(14, 'Kota Palangka Raya', 22),
(1, 'Kabupaten Berau', 23),
(2, 'Kabupaten Bulungan', 23),
(3, 'Kabupaten Kutai Barat', 23),
(4, 'Kabupaten Kutai Kartanegara', 23),
(5, 'Kabupaten Kutai Timur', 23),
(6, 'Kabupaten Malinau', 23),
(7, 'Kabupaten Nunukan', 23),
(8, 'Kabupaten Paser', 23),
(9, 'Kabupaten Penajam Paser Utara', 23),
(10, 'Kabupaten Tana Tidung', 23),
(11, 'Kota Balikpapan', 23),
(12, 'Kota Bontang', 23),
(13, 'Kota Samarinda', 23),
(14, 'Kota Tarakan', 23),
(1, 'Kabupaten Boalemo', 24),
(2, 'Kabupaten Bone Bolango', 24),
(3, 'Kabupaten Gorontalo', 24),
(4, 'Kabupaten Gorontalo Utara', 24),
(5, 'Kabupaten Pohuwato', 24),
(6, 'Kota Gorontalo', 24),
(1, 'Kabupaten Bantaeng', 25),
(2, 'Kabupaten Barru', 25),
(3, 'Kabupaten Bone', 25),
(4, 'Kabupaten Bulukumba', 25),
(5, 'Kabupaten Enrekang', 25),
(6, 'Kabupaten Gowa', 25),
(7, 'Kabupaten Jeneponto', 25),
(8, 'Kabupaten Kepulauan Selayar', 25),
(9, 'Kabupaten Luwu', 25),
(10, 'Kabupaten Luwu Timur', 25),
(11, 'Kabupaten Luwu Utara', 25),
(12, 'Kabupaten Maros', 25),
(13, 'Kabupaten Pangkajene dan Kepulauan', 25),
(14, 'Kabupaten Pinrang', 25),
(15, 'Kabupaten Sidenreng Rappang', 25),
(16, 'Kabupaten Sinjai', 25),
(17, 'Kabupaten Soppeng', 25),
(18, 'Kabupaten Takalar', 25),
(19, 'Kabupaten Tana Toraja', 25),
(20, 'Kabupaten Toraja Utara', 25),
(21, 'Kabupaten Wajo', 25),
(22, 'Kota Makassar', 25),
(23, 'Kota Palopo', 25),
(24, 'Kota Parepare', 25),
(1, 'Kabupaten Bombana', 26),
(2, 'Kabupaten Buton', 26),
(3, 'Kabupaten Buton Utara', 26),
(4, 'Kabupaten Kolaka', 26),
(5, 'Kabupaten Kolaka Utara', 26),
(6, 'Kabupaten Konawe', 26),
(7, 'Kabupaten Konawe Selatan', 26),
(8, 'Kabupaten Konawe Utara', 26),
(9, 'Kabupaten Muna', 26),
(10, 'Kabupaten Wakatobi', 26),
(11, 'Kota Bau-Bau', 26),
(12, 'Kota Kendari', 26),
(1, 'Kabupaten Banggai', 27),
(2, 'Kabupaten Banggai Kepulauan', 27),
(3, 'Kabupaten Buol', 27),
(4, 'Kabupaten Donggala', 27),
(5, 'Kabupaten Morowali', 27),
(6, 'Kabupaten Parigi Moutong', 27),
(7, 'Kabupaten Poso', 27),
(8, 'Kabupaten Tojo Una-Una', 27),
(9, 'Kabupaten Toli-Toli', 27),
(10, 'Kabupaten Sigi', 27),
(11, 'Kota Palu', 27),
(1, 'Kabupaten Bolaang Mongondow', 28),
(2, 'Kabupaten Bolaang Mongondow Selatan', 28),
(3, 'Kabupaten Bolaang Mongondow Timur', 28),
(4, 'Kabupaten Bolaang Mongondow Utara', 28),
(5, 'Kabupaten Kepulauan Sangihe', 28),
(6, 'Kabupaten Kepulauan Siau Tagulandang Biaro', 28),
(7, 'Kabupaten Kepulauan Talaud', 28),
(8, 'Kabupaten Minahasa', 28),
(9, 'Kabupaten Minahasa Selatan', 28),
(10, 'Kabupaten Minahasa Tenggara', 28),
(11, 'Kabupaten Minahasa Utara', 28),
(12, 'Kota Bitung', 28),
(13, 'Kota Kotamobagu', 28),
(14, 'Kota Manado', 28),
(15, 'Kota Tomohon', 28),
(1, 'Kabupaten Majene', 29),
(2, 'Kabupaten Mamasa', 29),
(3, 'Kabupaten Mamuju', 29),
(4, 'Kabupaten Mamuju Utara', 29),
(5, 'Kabupaten Polewali Mandar', 29),
(1, 'Kabupaten Buru', 30),
(2, 'Kabupaten Buru Selatan', 30),
(3, 'Kabupaten Kepulauan Aru', 30),
(4, 'Kabupaten Maluku Barat Daya', 30),
(5, 'Kabupaten Maluku Tengah', 30),
(6, 'Kabupaten Maluku Tenggara', 30),
(7, 'Kabupaten Maluku Tenggara Barat', 30),
(8, 'Kabupaten Seram Bagian Barat', 30),
(9, 'Kabupaten Seram Bagian Timur', 30),
(10, 'Kota Ambon', 30),
(11, 'Kota Tual', 30),
(1, 'Kabupaten Halmahera Barat', 31),
(2, 'Kabupaten Halmahera Tengah', 31),
(3, 'Kabupaten Halmahera Utara', 31),
(4, 'Kabupaten Halmahera Selatan', 31),
(5, 'Kabupaten Kepulauan Sula', 31),
(6, 'Kabupaten Halmahera Timur', 31),
(7, 'Kabupaten Pulau Morotai', 31),
(8, 'Kota Ternate', 31),
(9, 'Kota Tidore Kepulauan', 31),
(1, 'Kabupaten Asmat', 32),
(2, 'Kabupaten Biak Numfor', 32),
(3, 'Kabupaten Boven Digoel', 32),
(4, 'Kabupaten Deiyai', 32),
(5, 'Kabupaten Dogiyai', 32),
(6, 'Kabupaten Intan Jaya', 32),
(7, 'Kabupaten Jayapura', 32),
(8, 'Kabupaten Jayawijaya', 32),
(9, 'Kabupaten Keerom', 32),
(10, 'Kabupaten Kepulauan Yapen', 32),
(11, 'Kabupaten Lanny Jaya', 32),
(12, 'Kabupaten Mamberamo Raya', 32),
(13, 'Kabupaten Mamberamo Tengah', 32),
(14, 'Kabupaten Mappi', 32),
(15, 'Kabupaten Merauke', 32),
(16, 'Kabupaten Mimika', 32),
(17, 'Kabupaten Nabire', 32),
(18, 'Kabupaten Nduga', 32),
(19, 'Kabupaten Paniai', 32),
(20, 'Kabupaten Pegunungan Bintang', 32),
(21, 'Kabupaten Puncak', 32),
(22, 'Kabupaten Puncak Jaya', 32),
(23, 'Kabupaten Sarmi', 32),
(24, 'Kabupaten Supiori', 32),
(25, 'Kabupaten Tolikara', 32),
(26, 'Kabupaten Waropen', 32),
(27, 'Kabupaten Yahukimo', 32),
(28, 'Kabupaten Yalimo', 32),
(29, 'Kota Jayapura', 32),
(1, 'Kabupaten Fakfak', 33),
(2, 'Kabupaten Kaimana', 33),
(3, 'Kabupaten Manokwari', 33),
(4, 'Kabupaten Maybrat', 33),
(5, 'Kabupaten Raja Ampat', 33),
(6, 'Kabupaten Sorong', 33),
(7, 'Kabupaten Sorong Selatan', 33),
(8, 'Kabupaten Tambrauw', 33),
(9, 'Kabupaten Teluk Bintuni', 33),
(10, 'Kabupaten Teluk Wondama', 33),
(11, 'Kota Sorong', 33);

-- --------------------------------------------------------

--
-- Table structure for table `News`
--

CREATE TABLE `News` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `description` varchar(1000) NOT NULL,
  `detail` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `News`
--

INSERT INTO `News` (`id`, `title`, `date`, `description`, `detail`, `category`, `image`) VALUES
(1, 'Java EE at a Glance', '2017-02-15 00:00:00', 'Java Platform, Enterprise Edition (Java EE) is the standard in community-driven enterprise software. ', 'Java Platform, Enterprise Edition (Java EE) is the standard in community-driven enterprise software. Java EE is developed using the Java Community Process, with contributions from industry experts, commercial and open source organizations, Java User Groups, and countless individuals. Each release integrates new features that align with industry needs, improves application portability, and increases developer productivity.\r\n\r\nToday, Java EE offers a rich enterprise software platform and with over 20 compliant Java EE implementations to choose from, low risk and plenty of options.', 'Tech', 'http://www.oracle.com/ocom/groups/public/@otn/documents/webcontent/1957730.gif'),
(2, 'Android (operating system)', '2017-02-15 00:00:00', 'Android (stylized as android) is a mobile operating system developed by Google, based on the Linux kernel and designed primarily for touchscreen mobile devices such as smartphones and tablets.', 'Android''s user interface is mainly based on direct manipulation, using touch gestures that loosely correspond to real-world actions, such as swiping, tapping and pinching, to manipulate on-screen objects, along with a virtual keyboard for text input. In addition to touchscreen devices, Google has further developed Android TV for televisions, Android Auto for cars, and Android Wear for wrist watches, each with a specialized user interface. Variants of Android are also used on notebooks, game consoles, digital cameras, and other electronics.\r\n\r\nAndroid has the largest installed base of all operating systems (OS) of any kind.[b] Android has been the best selling OS on tablets since 2013, and on smartphones it is dominant by any metric.[15][16]', 'World', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Android_Nougat_screenshot_20170116-070000.png/300px-Android_Nougat_screenshot_20170116-070000.png');

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE `propinsi` (
  `propinsi_id` tinyint(4) NOT NULL,
  `propinsi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`propinsi_id`, `propinsi`) VALUES
(1, 'Aceh'),
(2, 'Sumatera Utara'),
(3, 'Bengkulu'),
(4, 'Jambi'),
(5, 'Riau'),
(6, 'Sumatera Barat'),
(7, 'Sumatera Selatan'),
(8, 'Lampung'),
(9, 'Kepulauan Bangka Belitung'),
(10, 'Kepulauan Riau'),
(11, 'Banten'),
(12, 'Jawa Barat'),
(13, 'DKI Jakarta'),
(14, 'Jawa Tengah'),
(15, 'Jawa Timur'),
(16, 'Daerah Istimewa Yogyakarta'),
(17, 'Bali'),
(18, 'Nusa Tenggara Barat'),
(19, 'Nusa Tenggara Timur'),
(20, 'Kalimantan Barat'),
(21, 'Kalimantan Selatan'),
(22, 'Kalimantan Tengah'),
(23, 'Kalimantan Timur'),
(24, 'Gorontalo'),
(25, 'Sulawesi Selatan'),
(26, 'Sulawesi Tenggara'),
(27, 'Sulawesi Tengah'),
(28, 'Sulawesi Utara'),
(29, 'Sulawesi Barat'),
(30, 'Maluku'),
(31, 'Maluku Utara'),
(32, 'Papua'),
(33, 'Papua Barat');

-- --------------------------------------------------------

--
-- Table structure for table `TB_CRUD`
--

CREATE TABLE `TB_CRUD` (
  `ID_CRUD` int(11) NOT NULL,
  `NAMA_CRUD` varchar(100) NOT NULL,
  `ALAMAT_CRUD` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE `tb_login` (
  `id` int(11) NOT NULL,
  `nama` varchar(300) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(300) NOT NULL,
  `password` text NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_login`
--

INSERT INTO `tb_login` (`id`, `nama`, `date`, `username`, `password`, `jabatan`) VALUES
(1, 'Muhammad Firman Akbar', '2017-02-16 06:35:29', 'firman', '74bfebec67d1a87b161e5cbcf6f72a4a', 'Admin'),
(2, 'Hani Devinta Sari', '2017-02-16 07:31:45', 'hani', '76e105c3a61db1b3f13207774aeccc3c', 'User'),
(12, 'a', '2017-02-17 07:32:38', 'a', '0cc175b9c0f1b6a831c399e269772661', 'User'),
(13, 'b', '2017-02-17 07:33:49', 'b', '92eb5ffee6ae2fec3ad71c777531578f', 'Admin'),
(14, 'qwQ', '2017-02-19 11:49:34', 'qw', '3ff01b4f2990f1fa7f31c82fd7c6c9d6', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tamu`
--

CREATE TABLE `tb_tamu` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tamu`
--

INSERT INTO `tb_tamu` (`id`, `time`, `nama`, `email`, `url`, `pesan`) VALUES
(1, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(2, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(3, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(5, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(6, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(7, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(8, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(9, '2017-01-17 15:29:28', 'caugiux', 'iug@kbx', 'tuygusx', 'skjbvsiobiox'),
(10, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(11, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(12, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(13, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(14, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(15, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(16, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(17, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(18, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(19, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(20, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(21, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(22, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(23, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(24, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(25, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(26, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(27, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(28, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(29, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(30, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(31, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(32, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(33, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(34, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(35, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(36, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(37, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(38, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(39, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(40, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(41, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(42, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(43, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(44, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(45, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(46, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(47, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(48, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(49, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(50, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(51, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(52, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(53, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(54, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(55, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(56, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(57, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(58, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(59, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(60, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(61, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(62, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(63, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(64, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(65, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(66, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(67, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(68, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(69, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(70, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(71, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(72, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(73, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(74, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(75, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(76, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(77, '2017-01-17 15:29:07', 'koko', 'k@k', 'gugyu', 'acasc'),
(78, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(79, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(80, '2017-01-17 14:37:00', 'firmanx', 'firman.programmer@gmail.com', 'google.com', 'Tes pesan buku tamu'),
(82, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(83, '2017-01-17 15:29:28', 'caugiu', 'iug@kb', 'tuygus', 'skjbvsiobio'),
(88, '2017-01-21 20:13:09', 'popo', 'e@e', 'popo.com', 'obvaucbai'),
(89, '2017-01-21 20:13:17', 'popo', 'e@e', 'popo.com', 'obvaucbai'),
(90, '2017-01-21 20:13:32', 'bb', 'b@b', 'bbb', 'bbbbbaicgai'),
(91, '2017-01-22 16:02:21', 'd', 'dev@dev', 'd.com', 'skso kao'),
(93, '2017-01-22 17:20:00', 'Firman Akbarx', 'm.firman.aa@gmail.comx', 'toriatec.comx', 'asking somethingx');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `status`) VALUES
(1, 'firman', '12345', 'On'),
(2, 'Akbar', '23412', 'Off'),
(3, 'kokoyeh', '7845', 'On'),
(4, 'kox', 'cax', 'onnx'),
(5, 'eee', 'rrrr', 'tttt');

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id` int(11) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token_api` varchar(300) DEFAULT NULL,
  `session_id` varchar(300) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_token_session` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id`, `email`, `password`, `token_api`, `session_id`, `create_date`, `update_token_session`) VALUES
(5, 'k@k.com', '61e2c054559da82ee780ca2b4bc6b3a5', 'a7546cb12ebaaf9475c4631416e8a14d2c1be170', 'd30d893af2d1823a7e087af95972842c54977d72', '2017-02-05 13:04:07', '2017-02-15 15:22:22'),
(9, 'f@f.com', '76668e33137eef14483e078c0dd57448', '67a74306b06d0c01624fe0d0249a570f4d093747', '2d47436625a76b11e5eca1b94e55e2aa98d5f725', '2017-02-05 13:04:07', '2017-02-05 07:33:49'),
(10, 'g@g.com', 'bab170ec41ac045d8c679f0ca43fdc41', '8cb7bf18c6f38825bf09b928d37b0482a8e8409a', '7e37725f36301f095ad9e28a9b2fb0512391af55', '2017-02-06 06:07:26', '2017-02-06 01:00:11'),
(11, 'p@g.com', 'bab170ec41ac045d8c679f0ca43fdc41', NULL, NULL, '2017-02-06 08:41:43', NULL),
(12, 'p@ga.com', 'bab170ec41ac045d8c679f0ca43fdc41', NULL, NULL, '2017-02-06 08:41:54', NULL),
(13, 'x@x', '218b547c84200f8955061c704092a0c4', NULL, NULL, '2017-02-06 08:49:07', NULL),
(14, 'r@r.com', '604a2592fdc946151a05706ed0d9978f', NULL, NULL, '2017-02-06 14:30:03', NULL),
(15, 'v@v', '86fb3b8aa400c037f999822aa309657c', NULL, NULL, '2017-02-06 14:45:43', NULL),
(16, 'xb', 'd436adeaeb56e877a12226260dede661', NULL, NULL, '2017-02-06 15:00:18', NULL),
(17, 'hi@hi', '7e2a0d63c7ccfc158df3362ad71c371e', NULL, NULL, '2017-02-06 15:04:29', NULL),
(18, 'hi@hu', 'b523e15fb99249f2245beb027dacb694', NULL, NULL, '2017-02-06 15:08:03', NULL),
(19, 'x', '9dd4e461268c8034f5c8564e155c67a6', NULL, NULL, '2017-02-07 08:29:27', NULL),
(20, 'w', '9e3669d19b675bd57058fd4664205d2a', NULL, NULL, '2017-02-07 08:29:33', NULL),
(21, 'jfb', '10f9132e586392a6513eeeb303db630c', NULL, NULL, '2017-02-07 08:29:59', NULL),
(22, 'q1', 'bab170ec41ac045d8c679f0ca43fdc41', NULL, NULL, '2017-02-06 08:41:43', NULL),
(23, 'q2', 'bab170ec41ac045d8c679f0ca43fdc41', NULL, NULL, '2017-02-06 08:41:54', NULL),
(24, 'q3', '218b547c84200f8955061c704092a0c4', NULL, NULL, '2017-02-06 08:49:07', NULL),
(25, 'q4', '604a2592fdc946151a05706ed0d9978f', NULL, NULL, '2017-02-06 14:30:03', NULL),
(26, 'q5', '86fb3b8aa400c037f999822aa309657c', NULL, NULL, '2017-02-06 14:45:43', NULL),
(27, 'q6', 'd436adeaeb56e877a12226260dede661', NULL, NULL, '2017-02-06 15:00:18', NULL),
(28, 'q7', '7e2a0d63c7ccfc158df3362ad71c371e', NULL, NULL, '2017-02-06 15:04:29', NULL),
(29, 'q8', 'b523e15fb99249f2245beb027dacb694', NULL, NULL, '2017-02-06 15:08:03', NULL),
(30, 'q9', '9dd4e461268c8034f5c8564e155c67a6', NULL, NULL, '2017-02-07 08:29:27', NULL),
(31, 'q10', '9e3669d19b675bd57058fd4664205d2a', NULL, NULL, '2017-02-07 08:29:33', NULL),
(32, 'q11', '10f9132e586392a6513eeeb303db630c', NULL, NULL, '2017-02-07 08:29:59', NULL),
(33, 'a1', 'a1', NULL, NULL, '2017-02-07 08:31:32', NULL),
(34, 'a2save', 'a1', '', '', '2017-02-07 08:34:50', '0000-00-00 00:00:00'),
(35, 'a2sadsvrve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(36, 'a2srkehkerytagave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(37, 'a2sagsve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(38, 'a2saagrve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(39, 'a2sayjve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(40, 'a2saesgrve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(41, 'a2savejs', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(42, 'a2sav6jye', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(43, 'a2satjsve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(44, 'a2ysave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(45, 'a2ssgave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(46, 'a27save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(47, '36a2save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(48, 'a2qsave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(49, 'a2savhtrse', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(50, 'a2strsave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(51, 'a2sadgmve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(52, 'a2sa35yve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(53, 'a2sa43ve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(54, 'a2sa6ve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(55, 'a2syyurave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(56, 'a2245save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(57, 'a24save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(58, 'yea2save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(59, 'a2sagrve', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(60, 'a2segwave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(61, 'a2wsave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(62, 'a2ssave', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(63, 'am2save', 'a1', '', '', '2017-02-07 08:35:50', '0000-00-00 00:00:00'),
(64, 'vb', '8bbc2b904d0f41c51ae92c2268935b03', NULL, NULL, '2017-02-08 14:59:14', NULL),
(65, 'kz@kwz.com', '594f803b380a41396ed63dca39503542', NULL, NULL, '2017-02-26 13:44:55', NULL),
(66, 'kz@kwzs.com', '594f803b380a41396ed63dca39503542', NULL, NULL, '2017-02-27 16:34:12', NULL),
(67, '23kz@kwzs.com', '594f803b380a41396ed63dca39503542', NULL, NULL, '2017-02-27 17:56:26', NULL),
(68, '23skz@kwzs.com', '594f803b380a41396ed63dca39503542', NULL, NULL, '2017-02-27 18:35:59', NULL),
(69, '23skz@kwzxs.com', '594f803b380a41396ed63dca39503542', NULL, NULL, '2017-02-27 22:05:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_visit`
--

CREATE TABLE `tb_visit` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_visit`
--

INSERT INTO `tb_visit` (`id`, `fname`, `lname`, `email`) VALUES
(1, 'Hani', 'DS', 'hani@gmail'),
(2, 'Firman', 'Akbar', 'g@g'),
(3, 'Muhx', 'Anamx', 'r@x'),
(4, 'Putri', 'BM', 'b@h'),
(6, 'koko', 'popo', 'ropo@er'),
(8, 'gogo', 'koko', 'k@k');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily`
--
ALTER TABLE `daily`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id_guest`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota_kabupaten`
--
ALTER TABLE `kota_kabupaten`
  ADD KEY `kota_id` (`kota_id`,`propinsi_id`);

--
-- Indexes for table `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propinsi`
--
ALTER TABLE `propinsi`
  ADD PRIMARY KEY (`propinsi_id`);

--
-- Indexes for table `TB_CRUD`
--
ALTER TABLE `TB_CRUD`
  ADD PRIMARY KEY (`ID_CRUD`);

--
-- Indexes for table `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_tamu`
--
ALTER TABLE `tb_tamu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tb_visit`
--
ALTER TABLE `tb_visit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily`
--
ALTER TABLE `daily`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id_guest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `News`
--
ALTER TABLE `News`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_login`
--
ALTER TABLE `tb_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_tamu`
--
ALTER TABLE `tb_tamu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `tb_visit`
--
ALTER TABLE `tb_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
