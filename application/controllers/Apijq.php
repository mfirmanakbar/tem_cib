<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apijq extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('model_api');
	}

	function index(){
		$limit_show = 10;
		$page = $this->input->get("page");
		$hasil = $this->model_api->jq_users($page,$limit_show);
		$total_all = $this->model_api->jq_users_counting();
		$total_page = ceil($total_all/$limit_show);
	    if($hasil){
	        $response = array(
	            'datax' => $hasil,
							'total_allx' => $total_all,
							'totalpagex' => $total_page,
	            'success' => true,
							'message' => 'Semua data users berhasil ditampilkan'
	            );
	    }else{
	        $response = array(
	            'data_resultx' => null,
	            'success' => false,
							'message' => 'Data Gagal ditampilkan!'
	            );
	    }
	    $this->output
		    ->set_status_header(200)
		    ->set_content_type('application/json', 'utf-8')
		    ->set_output(json_encode($response, JSON_PRETTY_PRINT))
		    ->_display();
	    exit;
	}

	function user_regis(){
        
		$post = json_decode(file_get_contents('php://input'));
		if(!empty($post->email)){
				$data = array(
						'email' => $post->email,
						'password' => md5($post->password)
				);
				$hasil = $this->model_api->user_register($data);
				if(!empty($hasil)){
						$response = array(
								'content' => $hasil,
								'status' => true,
								'message' => 'Terima kasih, Akun Anda sudah terdaftar!');
				}else{
						$response = array(
								'content'=>null,
								'status' => false,
								'message' => 'Email tersebut sudah terdaftar!');
				}
		}else {
			$response = array(
					'status' => false,
					'message' => 'Mohon lengkapi data form!');
		}
		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT))
			->_display();
		exit;
        
	}

}
