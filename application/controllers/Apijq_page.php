<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apijq_page extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data = array(
				'title' => 'Users',
			);
		$this->load->view('page_users', $data);
	}

	function page_users_add(){
		$data = array(
				'title' => 'Users',
			);
		$this->load->view('page_users_add', $data);
	}

}
