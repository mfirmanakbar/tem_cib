<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $title;?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" type="image/ico" href="<?php echo base_url('assets/img/logo1.png')?>">
		<link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/navbar-fixed-top.css')?>" rel="stylesheet">
	</head>
	<body>

		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" style="background-color:#ffffff;color:#000000;">
			<div class="row">
				<div class="text-center" style="background-color:#00c853;color:#ffffff;padding-top:10px; padding-bottom:10px; font-size:1.2em;">
					Template CI_3 Bootstrap_3
				</div>
			</div>
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img width="70%" src="<?php echo base_url('assets/img/logo2.png')?>"></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<?php echo site_url('home');?>">Home</a></li>
						<li><a href="<?php echo site_url('all-users');?>">Users</a></li>
						<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Report <span class="caret"></span></a>
              <ul class="dropdown-menu">
								<li class="dropdown-header">Export to:</li>
                <li><a href="<?php echo site_url('home/excel');?>" target="_blank">Excel</a></li>
                <li><a href="<?php echo site_url('home/pdf');?>" target="_blank">PDF</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="http://toriatec.com/" target="_blank">www.toriatec.com</a></li>
              </ul>
            </li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- Fixed navbar -->

		<br/><br/><br/>

		<div class="container">
			<!--Search-->
			<div class="row">
				<div class="col-xs-12 col-md-6 col-lg-8">
				</div>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<form class="form" method="get" action="cari.php"> <!-- form-inline -->
						<div class="form-group">
							<div class="input-group">
										<input type="text" class="form-control" id="txtcari" name="txtcari" value="" placeholder="Type here and press Enter">
								<div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!--Search-->

			<div class="row">
				<table class="table table-hover">
	        <thead>
	          <tr>
	              <td class="text-left"><b>Id</b></td>
	              <td class="text-left"><b>Date</b></td>
	              <td class="text-left"><b>Email</b></td>
	              <td class="text-left">
									<a href="<?php echo base_url('new-users')?>" class="btn btn-success" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
	              </td>
	          </tr>
	        </thead>
	        <tbody>
					</tbody>
	      </table>
				<div id="tbodyss"></div>
				<ul id="pagination_users" class="pagination-sm"></ul>
			</div>
		</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

<script src="<?php echo base_url('assets/js/item-ajax.js')?>"></script>

	</body>
</html>
