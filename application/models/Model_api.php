<?php if(!defined('BASEPATH')) exit('Hacking Attempt: Out now...!');

class Model_api extends CI_Model{

  function __construct(){
    parent::__construct();
  }

  function user_register($data){
    $this->db->where('email',$data['email']);
    $q = $this->db->count_all_results('tb_users');
    if($q > 0){
        unset($data);
        $data = null;
    }
    else{
        $this->db->insert('tb_users', $data);
        $data['id'] =$this->db->insert_id();
    }
    return $data;
  }

  function user_login($data){
      $this->db->where('email',$data['email']);
      $this->db->where('password',$data['password']);
      $q = $this->db->count_all_results('tb_users');
      if($q > 0){
        $session_id = sha1(md5($data['email'].$data['password']));
        $data['session_id'] = $session_id;
        $token_api = sha1(md5($data['email']));
        $data['token_api'] = $token_api;
        $this->db->where('email', $data['email']);
        $this->db->update('tb_users', array('token_api'=>$token_api,'session_id'=>$session_id,'update_token_session'=>date('Y-m-d H:i:s')));
      }
      else{
          unset($data);
          $data = null;
      }
      return $data;
  }

  function update_password($data){
      $this->db->where('email',$data['email']);
      $this->db->where('password',$data['old_password']);
      $q = $this->db->count_all_results('tb_users');
      if($q > 0){
        $this->db->where('email', $data['email']);
        $this->db->update('tb_users', array('password'=>$data['new_password']));
      }
      else{
          unset($data);
          $data = null;
      }
      return $data;
  }

  function all_users(){
		return $this->db->get('tb_users')->result();
	}

  function one_user($email){
    $this->db->where('email',$email);
		return $this->db->get('tb_users')->result_array();
	}

  public function cek_header($session_id,$token_api){
    $this->db->where('session_id',$session_id);
    $this->db->where('token_api',$token_api);
    $q = $this->db->count_all_results('tb_users');
    if($q != 0){
        $data['cek_header'] = 1;
    }else{
        $data['cek_header'] = 0;
    }
    return $data['cek_header'];
  }

  function delete_user($email){
      $this->db->delete('tb_users', array('email' => $email));
  }

  function pagination_user($page){
    $num_rec_per_page = 7;
    $start_from = ($page-1) * $num_rec_per_page;
    return $hasil = $this->db->query("SELECT * FROM tb_users ORDER by id desc LIMIT $start_from, $num_rec_per_page")->result_array();
  }


/*New API for APIJQ*/

  function jq_users($page,$limit_show){
    $this->db->order_by("id", "desc");
    $this->db->limit($limit_show, ($page - 1) * $limit_show);
    return $this->db->get("tb_users")->result();
  }
  function jq_users_counting(){
    return $this->db->count_all("tb_users");
  }


}
