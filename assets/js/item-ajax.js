var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;
var url = "http://localhost/tem_cib/apijq";
var url_regis = url+'/user_regis';


manageData();

/* manage data list */
function manageData() {
   $.ajax({
      dataType: 'json',
      url: url,
      data: {page:page}
    }).done(function(data){
        total_page = data.totalpagex;
       current_page = page;
       $('#pagination_users').twbsPagination({
            totalPages: total_page,
            visiblePages: current_page,
            onPageClick: function (event, pageL) {
                page = pageL;
                if(is_ajax_fire != 0){
                   getPageData();
                }
            }
        });
        manageRow(data.datax);
        is_ajax_fire = 1;
   });
}


/* Get Page Data*/
function getPageData() {
    $.ajax({
       dataType: 'json',
       url: url,
       data: {page:page}
	}).done(function(data){
       manageRow(data.datax);
    });
}

/* Add new Item table row */
function manageRow(data) {
    var	rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.id+'</td>';
        rows = rows + '<td>'+value.create_date+'</td>';
        rows = rows + '<td>'+value.email+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("tbody").html(rows);
    $("#tbodyss").html("<b>Hello world!</b>");
}

//$("#btnSimpanAdd").click(function(e){
//  e.preventDefault();//fungsinya untuk mencegah perpindahan page dari HREF misal
//  var EmailAdd = $("#txtEmailAdd").val();
//  var PasswordAdd = $("#txtPasswordAdd").val();
//  $.ajax({
//        dataType: 'json',
//        type:'POST',
//        url: 'http://localhost/tem_cib/apijq/store',
//        data:{email:EmailAdd, password:PasswordAdd}
//  }).done(function(data){
//      ///getPageData();
//      //$(".modal").modal('hide');
//      toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
//  });
//});

$(document).on('submit', '#regis_form', function(event) {
	event.preventDefault();
	/* Act on the event */
  var EmailAdd = $('input[id=txtEmailAdd]');//$("#txtEmailAdd").val();
  var PasswordAdd = $('input[id=txtPasswordAdd]');;//$("#txtPasswordAdd").val();
  var status = true;
  alert(EmailAdd.val()+PasswordAdd.val());
	$.ajax({
		url: url_regis,
		type: 'POST',
		dataType: 'json',
		data: {
      email: EmailAdd.val(),
      password: PasswordAdd.val()
    },
		success: function(event){
			if(event.status == status){
				$('.msg').text('Sukses!');
        //window.location.href = 'http://localhost/tem_cib/all-users';
			}else{
				$('.msg').text('Gagal! '+event.message);
			}
		},
		error: function(event){
			alert('Error Ajax Request!');
		}
	});
});
